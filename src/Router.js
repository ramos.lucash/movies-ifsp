import { createStackNavigator } from 'react-navigation';

import Home from './screens/Home';
import Content from './screens/Content';

export default createStackNavigator({
   Home,
   Content
}, {
   initialRouteName: 'Home',
   navigationOptions: {
      headerStyle: {
         backgroundColor: '#333',
      },
      headerTintColor: 'white'
   }
});