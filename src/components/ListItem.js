import React from 'react';
import { TouchableOpacity, ImageBackground, Text } from 'react-native';

export default props => {
	const { navigation, item } = props;

	const navigate = () => {
		navigation.navigate('Content', { content: item });
	}

	return (
	<TouchableOpacity
		style={{
			width: '48%',
			flexDirection: 'column',
			marginBottom: 20,
		}}
		onPress={navigate}
	>
		<ImageBackground
			source={{ uri: item.Poster }}
			style={{ height: 220, justifyContent: 'flex-end' }}
			resizeMode="cover"
		>
			<Text
				style={{
					textAlign: 'center',
					color: 'white',
					backgroundColor: 'rgba(0,0,0,0.7)',
					padding: 5,
				}}
			>
				{item.Title}
			</Text>
		</ImageBackground>
	</TouchableOpacity>
)};
