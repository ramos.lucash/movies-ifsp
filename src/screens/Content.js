import React, { Component } from 'react';
import {
	StyleSheet,
	Text,
	View,
	ScrollView,
	Image,
} from 'react-native';

import { getById } from '../request';

export default class App extends Component {
	static navigationOptions = ({ navigation }) => ({
		title: navigation.getParam('content').Title,
	});

	state = {
		content: null,
	};

	componentWillMount() {
		const { navigation } = this.props;
		const content = navigation.getParam('content');
		this.setState({ content });
		getById(content.imdbID).then(res => this.setState({ content: res.data }))
	}

	render() {
		const { content } = this.state;
		return (
			<ScrollView
				style={styles.container}
				contentContainerStyle={{ paddingBottom: 50 }}
			>
				<View style={{ flexDirection: 'row', height: 250 }}>
					<View style={{ flex: 1 }}>
						<Image
							source={{
								uri:
									content.Poster,
							}}
							resizeMode="cover"
							style={{ flex: 1 }}
						/>
					</View>
					<View style={{ flex: 1, justifyContent: 'flex-end', paddingLeft: 5 }}>
						<Text style={styles.text}>{content.Released}</Text>
						<Text style={styles.text}>{content.Genre}</Text>
						<Text style={styles.text}>{content.Director}</Text>
					</View>
				</View>
				<View style={{ marginTop: 20 }}>
					<Text style={styles.text}>
						{content.Plot}
					</Text>
				</View>
			</ScrollView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#333',
		padding: 20,
	},
	input: {
		backgroundColor: 'white',
		width: '100%',
		color: '#333',
		fontSize: 16,
		paddingVertical: 5,
		paddingHorizontal: 10,
		borderRadius: 20,
		paddingRight: 70,
	},
	listItem: { width: '49%', marginBottom: 10 },
	text: { color: 'white', textAlign: 'justify' },
});
