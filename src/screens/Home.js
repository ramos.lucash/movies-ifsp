import React, { Component } from 'react';
import {
	StyleSheet,
	Text,
	View,
	TextInput,
	TouchableOpacity,
	ScrollView,
	ActivityIndicator,
} from 'react-native';

import { searchByTitle } from '../request';
import ListItem from '../components/ListItem';

export default class App extends Component {
	static navigationOptions = {
		title: 'Home',
	};

	state = {
		search: '',
		response: null,
		isLoading: false,
	};

	search = () => {
		const { search } = this.state;
		this.setState({ isLoading: true });
		searchByTitle(search).then(res =>
			this.setState({ response: res.data, isLoading: false }),
		);
	};

	render() {
		const { response, isLoading } = this.state;
		const { navigation } = this.props;

		if (isLoading)
			return (
				<View style={styles.container}>
					<ActivityIndicator size="large" />
				</View>
			);

		return (
			<ScrollView
				style={styles.container}
				contentContainerStyle={{ paddingBottom: 50 }}
			>
				<View style={{ flexDirection: 'row', alignItems: 'center' }}>
					<TextInput
						style={styles.input}
						placeholder="Search..."
						placeholderTextColor="#888"
						value={this.state.search}
						onChangeText={text => this.setState({ search: text })}
					/>
					<TouchableOpacity
						style={{
							paddingHorizontal: 10,
							position: 'absolute',
							right: 10,
							width: 50,
							alignItems: 'center',
							backgroundColor: '#ccc',
							borderRadius: 10,
						}}
						onPress={this.search}
					>
						<Text style={{ color: '#333' }}>Go!</Text>
					</TouchableOpacity>
				</View>
				{response && response.Error && (
					<Text style={{ marginTop: 20, color: 'white' }}>{esponse.Error}</Text>
				)}
				{response && response.totalResults && (
					<Text style={{ marginTop: 20, color: 'white' }}>
						{`${response.totalResults} results found`}
					</Text>
				)}
				<View
					style={{
						justifyContent: 'space-between',
						flexDirection: 'row',
						flexWrap: 'wrap',
						marginTop: 20,
					}}
				>
					{response &&
						response.Search &&
						response.Search.map(item => (
							<ListItem item={item} navigation={navigation} />
						))}
				</View>
			</ScrollView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#333',
		padding: 20,
	},
	input: {
		backgroundColor: 'white',
		width: '100%',
		color: '#333',
		fontSize: 16,
		paddingVertical: 5,
		paddingHorizontal: 10,
		borderRadius: 20,
		paddingRight: 70,
	},
	listItem: { width: '49%', marginBottom: 10 },
});
