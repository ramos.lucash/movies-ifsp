import axios from 'axios';

const instance = axios.create({
	baseURL: 'http://www.omdbapi.com',
});

const API_KEY = '?apikey=e8b3cc0a&';

export const searchByTitle = title =>
	instance.get(API_KEY + 's=' + title + '&type=movie');

export const getById = id => instance.get(`${API_KEY}i=${id}&plot=full`);

export default instance;
