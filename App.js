import React, { Component } from 'react';
import { StatusBar } from 'react-native';
import Router from './src/Router';

export default class App extends Component {
	constructor(props) {
		super(props);
		StatusBar.setBarStyle('light-content');
		StatusBar.setBackgroundColor('#333');
	}
	render() {
		return <Router />;
	}
}
